package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    @Query("select u from User u where u.lastName = : lastName order by u.lastName asc")
    List<User> findAllByLastName(String lastName, Pageable pageable);

    @Query("select u from User u where u.office.city = : city order by u.lastName asc")
    List<User> findAllByCity(String city);

    @Query("select u from User u where u.experience = : experience order by u.experience desc")
    List<User> findAllByExperience(int experience);

    @Query("select u from User u where u.city = : city and u.room = : room")
    List<User> findAllByRoomAndCity(String city, String room);
}
