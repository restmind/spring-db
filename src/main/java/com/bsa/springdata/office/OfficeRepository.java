package com.bsa.springdata.office;

import com.bsa.springdata.team.Technology;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {
    @Query("select u from Office u where u.users.team.technology : = technology")
    List<OfficeDto> findAllByTechnology(String technology);
}
