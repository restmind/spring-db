package com.bsa.springdata.office;

import com.bsa.springdata.user.User;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@Table(name = "offices")
public class Office {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private String city;

    @Column
    private String address;

    @OneToMany(mappedBy = "office", fetch = FetchType.LAZY)
    private List<User> users;
}
